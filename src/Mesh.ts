import { mat4, vec2, vec3, quat } from 'gl-matrix'

export class Mesh
{
    gl: WebGLRenderingContext
    verticesCount = 0
    vertices = Array<number>()
    vertexIndex = Array<number>()
    colors = Array<number>()
    uvs = Array<number>()
    nodes = { }
    name: string = ''

	constructor(gl: WebGLRenderingContext) {
		this.gl = gl;
    }

	static makeQuad(gl: WebGLRenderingContext, size: vec3, segments: vec2): Mesh {
        let w = size[0];
        let h = size[1];
        let z = size[2];
		let sx = Math.ceil(segments[0])
        let sy = Math.ceil(segments[1])
        let sw = w/sx;
        let sh = h/sy;
        let ox = -0.5 * w;
        let oy = -0.5 * h;
		var ku = 1/sw;
		var kv = 1/sh;
        var mesh = new Mesh(gl)
		for(var ny=0; ny<sy; ++ny) {
			for(var nx=0; nx<sx; ++nx) {
				var gx = ox + nx * sw;
				var gy = oy + ny * sh;
                mesh.vertices = mesh.vertices.concat([
					gx,    gy,    z,
					gx,    gy+sh, z,
					gx+sw, gy+sh, z,
					gx,    gy,    z,
					gx+sw, gy+sh, z,
					gx+sw, gy,    z,
				])
				var u = nx * ku;
                var v = ny * kv;
				mesh.uvs = mesh.uvs.concat([
					u,    v,
					u,    v+kv,
					u+ku, v+kv,
					u,    v,
					u+ku, v+kv,
					u+ku, v
				])
				mesh.verticesCount += 6;
				mesh.vertexIndex = mesh.vertexIndex.concat([1,2,3,4,5,6])
				mesh.colors = mesh.colors.concat(Array<number>(6*4).map(_=>1))
			}
        }
        return mesh
	}
}