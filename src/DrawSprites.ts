import { mat4, vec4, vec2, vec3 } from "gl-matrix";
import { Mesh } from "./Mesh";

export class Uniforms {
    modelViewProjectionMatrix = mat4.create() // 16
    normalMatrix = mat4.create() // 16
    color = vec4.fromValues(1, 1, 1, 1) // 4
    
    buffer = new Float32Array(16 + 16 + 4)

    copyToArrayBuffer(target: Float32Array | Array<number>, offset: number) {
        this.buffer.set(target, offset)
    }

    getArrayBuffer(): Float32Array {
        this.copyToArrayBuffer(this.modelViewProjectionMatrix, 0)
        this.copyToArrayBuffer(this.normalMatrix, this.modelViewProjectionMatrix.length)
        this.copyToArrayBuffer(this.color, this.normalMatrix.length)
        return this.buffer
    }
}

export class InstanceUniforms {
    size = vec2.fromValues(0, 0)
    transform = mat4.create()
    color = vec4.fromValues(1, 1, 1, 1)

    buffer = new Float32Array(16 + 16 + 4)

    copyToArrayBuffer(target: Float32Array | Array<number>, offset: number) {
        this.buffer.set(target, offset)
    }

    getArrayBuffer(): Float32Array {
        this.copyToArrayBuffer(this.size, 0)
        this.copyToArrayBuffer(this.transform, this.size.length)
        this.copyToArrayBuffer(this.color, this.transform.length)
        return this.buffer
    }
}

export class DrawSprites {

    // static UniformsBufferIndex = 0
    // static UniformsBufferLength = 65_536
    // static UniformsLength = (16 + 16 + 4)
    static MaxFramesInFlight = 1


    gl: WebGLRenderingContext
    ext: ANGLE_instanced_arrays
    mesh?: Mesh
    uniformsBuffers = Array<Uniforms>()
    instanceBuffers = Array<InstanceUniforms>()

    transformMatrix = mat4.create()
    projectionMatrix = mat4.create()
    viewMatrix = mat4.create()

    worldMatrix = mat4.create()
    globalTransform = mat4.create()


    constructor(gl: WebGLRenderingContext) {
        this.gl = gl;
        this.ext = gl.getExtension('ANGLE_instanced_arrays')!
        mat4.identity(this.worldMatrix)
        mat4.identity(this.globalTransform)
		mat4.translate(this.viewMatrix, this.viewMatrix, [0.0, 0.0, -100.000])
        this.setViewportSize(gl.drawingBufferWidth, gl.drawingBufferHeight)
        this.makeUniformsBuffers()
        this.makeInstanceBuffers()
        this.makeMesh()
    }

    setViewportSize(width: number, height: number) {
        let halfWidth = width / 2;
        let halfHeight = height / 2;
		mat4.ortho(this.projectionMatrix, -halfWidth, halfWidth, -halfHeight, halfHeight, -100, 100)
    }

    makeUniformsBuffers() {
        let { gl } = this;
        let constants = new Uniforms()
        mat4.mul(constants.modelViewProjectionMatrix, this.projectionMatrix, this.viewMatrix)
        mat4.mul(constants.modelViewProjectionMatrix, constants.modelViewProjectionMatrix, this.worldMatrix)
        let constantsBuffer = constants.getArrayBuffer()
        for(var i=0; i<DrawSprites.MaxFramesInFlight; ++i) {
            let buf = gl.createBuffer()
            if (!buf) { return }
            gl.bindBuffer(gl.ARRAY_BUFFER, buf)
            gl.bufferData(gl.ARRAY_BUFFER, constantsBuffer, gl.STATIC_DRAW)
        }
    }

    makeInstanceBuffers() {

    }

    makeMesh() {
        if (this.gl === null) { return }
        this.mesh = Mesh.makeQuad(this.gl, vec3.fromValues(2, 2, 1), vec2.fromValues(1, 1))
    }

}