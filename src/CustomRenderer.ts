export class CustomRenderer {

    canvas: HTMLCanvasElement
    gl: WebGLRenderingContext | null = null


    constructor(canvas: HTMLCanvasElement) {
        this.canvas = canvas;
    }

    createRenderContext2D(width: number, height: number) {
        this.canvas.width = width;
		this.canvas.height = height;
		this.canvas.style.backgroundColor = 'rgba(0,0,0,0)';
		try {
			var gl = this.gl = this.canvas.getContext('webgl')! || this.canvas.getContext('experimental-webgl')!
			if (gl === null) { return null }
			gl.disable(gl.DEPTH_TEST)
			gl.enable(gl.BLEND)
			gl.blendFuncSeparate(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA, gl.ONE, gl.ONE_MINUS_SRC_ALPHA)
			gl.clearColor(0, 0, 0, 0)
			gl.clearDepth(1.0)
			gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
		} catch(e) {
			alert('Unable to initialize WebGL. Your browser may not support it')
		}
		return this.gl;
    }

}