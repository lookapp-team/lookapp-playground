import { mat4, vec2, vec3, quat } from 'gl-matrix'

export class Transform {

    position: vec3
    scale: vec3
    quaternion: quat
    matrix: mat4
    draft: boolean
    parent?: Transform
    worldQuaternion: quat
    worldMatrix: mat4
    worldDraft: boolean


    constructor() {
		this.position = vec3.fromValues(0, 0, 0)
		this.scale = vec3.fromValues(1, 1, 1)
		this.quaternion = quat.create()
		this.matrix = mat4.create()
		this.draft = false;
		this.worldQuaternion = quat.create()
		this.worldMatrix = mat4.create()
		this.worldDraft = false;
    }

	setParent(transform: Transform) {
		this.parent = transform;
		this.draft = true;
	}

	update() {
		if (this.parent) {
			this.parent.update()
			if (this.parent.worldDraft || this.draft) {
				quat.mul(this.worldQuaternion, this.quaternion, this.parent.worldQuaternion)
				var p = vec3.create()
				var s = vec3.fromValues(1, 1, 1)
				mat4.getScaling(s, this.parent.worldMatrix)
				vec3.mul(s, s, this.scale)
				vec3.transformMat4(p, this.position, this.parent.worldMatrix)
				mat4.fromRotationTranslationScale(this.worldMatrix, this.worldQuaternion, p, s)
				this.worldDraft = true;
			}
		} else if (this.draft) {
			quat.copy(this.worldQuaternion, this.quaternion)
			mat4.copy(this.worldMatrix, this.matrix)
			this.worldDraft = true;
		}
    }
    
    translate(deltaX: number, deltaY: number, deltaZ: number) {
		var v = vec3.fromValues(deltaX, deltaY, deltaZ)
		vec3.add(this.position, this.position, v) 
		mat4.translate(this.matrix, this.matrix, v)
		this.draft = true;
    }
    
    setScale(scaleX: number, scaleY: number, scaleZ: number) {
		var v = vec3.fromValues(scaleX, scaleY, scaleZ)
		vec3.set(this.scale, v[0], v[1], v[2]) 
		mat4.scale(this.matrix, this.matrix, v)
		this.draft = true;
	}

	rotateX(rad: number) {
		quat.rotateZ(this.quaternion, this.quaternion, rad)
		this.draft = true;
	}

	rotateY(rad: number) {
		quat.rotateZ(this.quaternion, this.quaternion, rad)
		this.draft = true;
	}

	rotateZ(rad: number) {
		quat.rotateZ(this.quaternion, this.quaternion, rad)
		this.draft = true;
    }
    
    setRotation(x?: number, y?: number, z?: number) {
        var quaternion = quat.fromValues(this.quaternion[0], this.quaternion[1], this.quaternion[2], this.quaternion[3])
		if (x !== undefined)
			quat.rotateX(quaternion, quaternion, x)
		if (y !== undefined)
			quat.rotateY(quaternion, quaternion, y)
		if (z !== undefined)
			quat.rotateZ(quaternion, quaternion, z)
		mat4.fromRotationTranslationScale(this.matrix, quaternion, this.position, this.scale)
		this.draft = true;
    }
    
    setPosition(x?: number, y?: number, z?: number) {
		if (x !== undefined)
			this.position[0] = x;
        if (y !== undefined)
			this.position[1] = y;
		if (z !== undefined)
			this.position[2] = z;
		mat4.fromRotationTranslationScale(this.matrix, this.quaternion, this.position, this.scale)
		this.draft = true;
	}

}
