import { App } from './App'
import { CustomRendererView } from './CustomRendererView'
import { CustomRenderer } from './CustomRenderer'
import { Mesh } from './Mesh'
import { vec2, vec3 } from 'gl-matrix'

let app = new App()

let canvas: HTMLElement | null = document.getElementById('view')
if (!(canvas instanceof HTMLElement)) {
    canvas = document.createElement('canvas')
    document.body.appendChild(canvas)
}

let view = new CustomRendererView(canvas)
view.createRenderer()

let plane = Mesh.makeQuad(view.gl!, vec3.fromValues(100, 100, 1), vec2.fromValues(1, 1))

let fpsLabel = document.getElementById('fps')!
var fpsCount = 0;
var fpsLastTimestamp = performance.now()

requestAnimationFrame(function render() {
    let { gl } = view;
    
    fpsCount += 1;
    if (performance.now() - fpsLastTimestamp >= 1000) {
        fpsLastTimestamp = performance.now()
        fpsLabel.innerHTML = `${fpsCount}`;
        fpsCount = 0;
    }

    if (!view.pause && gl) {
        gl.clear(gl.COLOR_BUFFER_BIT) // | gl.DEPTH_BUFFER_BIT
    }

    requestAnimationFrame(render)
})