import { CustomRenderer } from './CustomRenderer'

export class CustomRendererView {

    canvas: HTMLElement
    renderer: CustomRenderer | null = null
    gl: WebGLRenderingContext | null = null
    pause = false


    constructor(canvas: HTMLElement) {
        this.canvas = canvas;
    }

    createRenderer() {
        var ratio =  this.canvas.clientWidth/this.canvas.clientHeight;
        this.renderer = new CustomRenderer(this.canvas as HTMLCanvasElement)
        this.gl = this.renderer.createRenderContext2D(ratio*1280, 1280)
        if (this.gl === null) { return }
        console.log('[gl] canvas size', this.canvas.clientWidth+'x'+this.canvas.clientHeight)
        console.log('[gl] render size', this.gl.drawingBufferWidth+'x'+this.gl.drawingBufferHeight)
        console.log('[gl] max', this.gl.getParameter(this.gl.MAX_COMBINED_TEXTURE_IMAGE_UNITS), 'combined textures')
    }

}